"syntax processing
syntax enable

"no of spaces per tab
set tabstop=4

" number of spaces in tab when editing
set softtabstop=4

set expandtab           " tabs are spaces

set number              " show line numbers

set showcmd             " show command in bottom bar

set cursorline          " highlight current line

set wildmenu            " visual autocomplete for command menu

set showmatch           " highlight matching braces

set incsearch           " search as characters are entered

set hlsearch            " highlight matches

" move to beginning/end of line
nnoremap B ^
nnoremap E $

" $/^ doesn't do anything
nnoremap $ <nop>
nnoremap ^ <nop>
